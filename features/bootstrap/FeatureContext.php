<?php

use Imbo\BehatApiExtension\Context\ApiContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends ApiContext
{
    private string $sellerFileName;
    private string $sellerFile;
    private string $productFileName;
    private string $productFile;
    private string $sellerProductFileName;
    private string $sellerProductFile;
    private string $cartFileName;
    private string $cartFile;

    public function __construct()
    {
        $this->sellerFileName = '/app/database/seller.json';
        $this->sellerFile = file_get_contents($this->sellerFileName);

        $this->productFileName = '/app/database/product.json';
        $this->productFile = file_get_contents($this->productFileName);

        $this->sellerProductFileName = '/app/database/seller_product.json';
        $this->sellerProductFile = file_get_contents($this->sellerProductFileName);

        $this->cartFileName = '/app/database/cart.json';
        $this->cartFile = file_get_contents($this->cartFileName);

    }

    /** @AfterScenario */
    public function teardown()
    {
        $file = fopen($this->sellerFileName, 'w');
        fwrite($file, $this->sellerFile);
        fclose($file);

        $file = fopen($this->productFileName, 'w');
        fwrite($file, $this->productFile);
        fclose($file);

        $file = fopen($this->sellerProductFileName, 'w');
        fwrite($file, $this->sellerProductFile);
        fclose($file);

        $file = fopen($this->cartFileName, 'w');
        fwrite($file, $this->cartFile);
        fclose($file);
    }
}
