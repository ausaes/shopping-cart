@cart
Feature: We use the add product endpoint
  Scenario: The request to get the amount
    When I request '/cart/amount'
    Then the response code is 200
    And the response body matches:
    """
    "amount"
    """