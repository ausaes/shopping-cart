@cart
Feature: We use the add product endpoint
  Scenario: The request with invalid product
    When I request '/cart/invalid-id' using HTTP DELETE
    Then the response code is 400
    And the response body contains JSON:
    """
      {
        "status": "error",
        "info": "Stock not found"
      }
    """
  Scenario: The request with in cart product
    When I request '/cart/the-new-id' using HTTP DELETE
    Then the response code is 202
