@cart @test
Feature: We use the confirm cart endpoint
  Scenario: The request with non empty cart
    When I request '/cart/confirm' using HTTP POST
    Then the response code is 201
    And the response body contains JSON:
    """
    {}
    """

  Scenario: The request with non empty cart
    When I request '/cart/confirm' using HTTP POST
    And  I request '/cart/confirm' using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Empty cart cannot be confirmed"
    }
    """