@product
Feature: We use the create product endpoint
  Scenario: The request without params
    Given the following form parameters are set:
      | name | value |
    When I request '/seller/valid-id/product' using HTTP POST
    Then the response code is 404


  Scenario: The request without product price param
    Given the following form parameters are set:
      | name | value |
      | productId | the-product-id |
      | productName | The product name |
    When I request '/seller/valid-id/product/the-id' using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Invalid product price"
    }
    """

  Scenario: The request with invalid seller id
    Given the following form parameters are set:
      | name | value |
      | productPrice | 10.20            |
    When I request '/seller/invalid-id/product/the-id' using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Seller not found"
    }
    """

  Scenario: The request with invalid seller id
    Given the following form parameters are set:
      | name | value |
      | productPrice | 10.20 |
    When I request '/seller/valid-id/product/invalid-id' using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Product not found"
    }
    """

  Scenario: The request with existent product id
    Given the following form parameters are set:
      | name | value |
      | productPrice | 10.20 |
    When I request '/seller/valid-id/product/the-new-id' using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Product and seller already linked"
    }
    """

  Scenario: The valid request
    Given the following form parameters are set:
      | name | value |
      | productPrice | 10.20 |
    When I request '/seller/valid-id/product/the-id' using HTTP POST
    Then the response code is 201