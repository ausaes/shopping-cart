@product
Feature: We use the decrease product endpoint
  Scenario: The request with invalid params
    When I request '/seller/invalid-id/product/invalid-id/decrease/1' using HTTP PATCH
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Product not found"
    }
    """

  Scenario: The request with invalid seller
    When I request '/seller/invalid-seller/product/the-id/decrease/1' using HTTP PATCH
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Product not found"
    }
    """

  Scenario: The request with invalid product
    When I request '/seller/valid-id/product/invalid-id/decrease/1' using HTTP PATCH
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Product not found"
    }
    """

  Scenario: The request with invalid product
    When I request '/seller/valid-id/product/the-new-id/decrease/one' using HTTP PATCH
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Invalid product quantity"
    }
    """

  Scenario: The request with invalid product
    When I request '/seller/valid-id/product/the-new-id/decrease/1' using HTTP PATCH
    Then the response code is 201
    And the response body contains JSON:
    """
    {}
    """