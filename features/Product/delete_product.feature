@product @delete
Feature: We use the delete product endpoint
  Scenario: The request with invalid params
    Given the following form parameters are set:
      | name | value |
    When I request '/seller/valid-id/product/the-id' using HTTP DELETE
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Product and seller are not linked"
    }
    """

  Scenario: The request with valid params
    Given the following form parameters are set:
      | name | value |
    When I request '/seller/valid-id/product/the-new-id' using HTTP DELETE
    Then the response code is 201