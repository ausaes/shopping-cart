Feature: We use the create seller endpoint
  Scenario: The request without params
    Given the following form parameters are set:
      | name | value |
    When I request '/seller' using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Invalid Id"
    }
    """

  Scenario: The request with only id param
    Given the following form parameters are set:
      | name | value |
      | id  | the id |
    When I request '/seller' using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Invalid name"
    }
    """

  Scenario: The request with an empty param
    Given the following form parameters are set:
      | name | value |
      | id  |  |
      | name | the name |
    When I request '/seller' using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Invalid Id"
    }
    """

  Scenario: The request with an empty param
    Given the following form parameters are set:
      | name | value |
      | id  | the id |
      | name | |
    When I request '/seller' using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Invalid name"
    }
    """

  Scenario: The request with valid params
    Given the following form parameters are set:
      | name | value |
      | id  | the new id |
      | name | the name |
    When I request '/seller' using HTTP POST
    Then the response code is 201