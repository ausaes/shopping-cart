Feature: We use the delete seller endpoint
  Scenario: The request without params
    When I request '/seller/' using HTTP DELETE
    Then the response code is 404

  Scenario: The request with invalid id param
    When I request '/seller/invalid-id' using HTTP DELETE
    Then the response code is 400
    And the response body contains JSON:
    """
    {
      "status": "error",
      "info": "Seller not found"
    }
    """

  Scenario: The request with valid id param
    When I request '/seller/valid-id' using HTTP DELETE
    Then the response code is 202