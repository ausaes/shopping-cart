<?php

namespace Unit\Product;

use App\ShoppingCart\Product\Domain\Repository\ProductRepository;
use App\ShoppingCart\Product\Domain\Service\DeleteProduct\DeleteProductCommand;
use App\ShoppingCart\Product\Domain\Service\DeleteProduct\DeleteProductCommandHandler;
use App\ShoppingCart\Product\Domain\ValueObject\Product;
use App\ShoppingCart\Seller\Domain\Repository\SellerRepository;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;
use PHPUnit\Framework\TestCase;

class DeleteProductTest extends TestCase
{
    /**
     * @test
     * @dataProvider invalidParamsDataProvider
     */
    public function givenAnInvalidParamsThenTheCommandThrowsAnException(
        $sellerId,
        $productId,
        string $expectedExceptionMessage
    ) {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($expectedExceptionMessage);
        new DeleteProductCommand($sellerId, $productId);
    }

    /** @test */
    public function givenAValidParamsThenItAddsToTheDatabaseAndUnlinksToSeller()
    {
        $productRepository = $this->getMockBuilder(ProductRepository::class)->getMock();
        $sellerProductRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $sellerRepository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $sellerRepository
            ->method('getSeller')
            ->willReturn(new Seller('seller-id', 'Seller name'));
        $productRepository
            ->method('getProduct')
            ->willReturn(new Product('product-id', 'Product name'));

        $sellerProductRepository
            ->expects($this->once())
            ->method('unlinkProductToSeller');

        $handler = new DeleteProductCommandHandler($productRepository, $sellerRepository, $sellerProductRepository);
        $handler(new DeleteProductCommand('seller-id', 'product-id'));
    }

    /** @test */
    public function givenAInvalidProductIdThenItThrowsException()
    {
        $productRepository = $this->getMockBuilder(ProductRepository::class)->getMock();
        $sellerProductRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $sellerRepository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $sellerRepository
            ->method('getSeller')
            ->willReturn(new Seller('seller-id', 'Seller name'));

        $productRepository
            ->method('getProduct')
            ->willReturn(null);


        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Product not found');
        $handler = new DeleteProductCommandHandler($productRepository, $sellerRepository, $sellerProductRepository);
        $handler(new DeleteProductCommand('seller-id', 'product-id'));
    }

    /** @test */
    public function givenAInvalidSellerIdThenItThrowsException()
    {
        $productRepository = $this->getMockBuilder(ProductRepository::class)->getMock();
        $sellerProductRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $sellerRepository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $sellerRepository
            ->method('getSeller')
            ->willReturn(null);

        $productRepository
            ->method('getProduct')
            ->willReturn(new Product('product-id', 'Product name'));


        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Seller not found');
        $handler = new DeleteProductCommandHandler($productRepository, $sellerRepository, $sellerProductRepository);
        $handler(new DeleteProductCommand('seller-id', 'product-id'));
    }

    public function invalidParamsDataProvider()
    {
        return [
            [1, 'product-id', 'Invalid seller id'],
            ['', 'product-id', 'Invalid seller id'],
            [null, 'product-id', 'Invalid seller id'],
            ['seller-id', 1, 'Invalid product id'],
            ['seller-id', '', 'Invalid product id'],
            ['seller-id', null, 'Invalid product id'],
        ];
    }
}
