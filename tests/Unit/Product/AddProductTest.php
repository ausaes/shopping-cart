<?php

namespace Unit\Product;

use App\ShoppingCart\Product\Domain\Repository\ProductRepository;
use App\ShoppingCart\Product\Domain\Service\AddProduct\AddProductCommand;
use App\ShoppingCart\Product\Domain\Service\AddProduct\AddProductCommandHandler;
use App\ShoppingCart\Product\Domain\ValueObject\Product;
use App\ShoppingCart\Seller\Domain\Repository\SellerRepository;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;
use PHPUnit\Framework\TestCase;

class AddProductTest extends TestCase
{
    /**
     * @test
     * @dataProvider invalidParamsDataProvider
     */
    public function givenAnInvalidParamsThenTheCommandThrowsAnException(
        $sellerId,
        $productId,
        $productPrice,
        string $expectedExceptionMessage
    ) {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($expectedExceptionMessage);
        new AddProductCommand($sellerId, $productId, $productPrice);
    }

    /** @test */
    public function givenAInvalidProductIdThenItThrowsException()
    {
        $productRepository = $this->getMockBuilder(ProductRepository::class)->getMock();
        $sellerProductRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $sellerRepository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $sellerRepository
            ->method('getSeller')
            ->willReturn(new Seller('seller-id', 'Seller name'));

        $productRepository
            ->method('getProduct')
            ->willReturn(null);


        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Product not found');
        $handler = new AddProductCommandHandler($productRepository, $sellerProductRepository, $sellerRepository);
        $handler(new AddProductCommand('seller-id', 'product-id', 10.0));
    }

    /** @test */
    public function givenAInvalidSellerIdThenItThrowsException()
    {
        $productRepository = $this->getMockBuilder(ProductRepository::class)->getMock();
        $sellerProductRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $sellerRepository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $sellerRepository
            ->method('getSeller')
            ->willReturn(null);

        $productRepository
            ->method('getProduct')
            ->willReturn(new Product('product-id', 'Product name'));


        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Seller not found');
        $handler = new AddProductCommandHandler($productRepository, $sellerProductRepository, $sellerRepository);
        $handler(new AddProductCommand('seller-id', 'product-id', 10.0));
    }
    /** @test */
    public function givenAValidParamsThenItLinks()
    {
        $productRepository = $this->getMockBuilder(ProductRepository::class)->getMock();
        $sellerProductRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $sellerRepository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $sellerRepository
            ->method('getSeller')
            ->willReturn(new Seller('seller-id', 'Seller name'));

        $productRepository
            ->method('getProduct')
            ->willReturn(new Product('product-id', 'Product name'));

        $sellerProductRepository
            ->expects($this->once())
            ->method('linkProductToSeller');

        $handler = new AddProductCommandHandler($productRepository, $sellerProductRepository, $sellerRepository);
        $handler(new AddProductCommand('seller-id', 'product-id', 10.0));
    }
    /** @test */
    public function givenAnInvalidSellerParamThenThrowsAnException()
    {
        $productRepository = $this->getMockBuilder(ProductRepository::class)->getMock();
        $sellerProductRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $sellerRepository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $sellerRepository
            ->method('getSeller')
            ->willReturn(null);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Seller not found');

        $handler = new AddProductCommandHandler($productRepository, $sellerProductRepository, $sellerRepository);
        $handler(new AddProductCommand('seller-id', 'product-id', 10.0));
    }

    public function invalidParamsDataProvider()
    {
        return
            [
                [1, 'product-id', 10.0, 'Invalid seller id'],
                ['', 'product-id', 10.0, 'Invalid seller id'],
                [null, 'product-id', 10.0, 'Invalid seller id'],
                ['seller-id', 1,  10.0, 'Invalid product id'],
                ['seller-id', '',  10.0, 'Invalid product id'],
                ['seller-id', null, 10.0, 'Invalid product id'],
                ['seller-id', 'product-id', null, 'Invalid product price'],
                ['seller-id', 'product-id', 'price', 'Invalid product price']
            ];
    }
}
