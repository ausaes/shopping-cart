<?php

namespace Unit\Product;

use App\ShoppingCart\Product\Domain\Service\DecreaseQuantity\DecreaseQuantityCommand;
use App\ShoppingCart\Product\Domain\Service\DecreaseQuantity\DecreaseQuantityCommandHandler;
use App\ShoppingCart\Product\Domain\ValueObject\Product;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;
use App\ShoppingCart\Shared\Domain\ValueObject\Stock;
use PHPUnit\Framework\TestCase;

class DecreaseProductQuantityTest extends TestCase
{
    /**
     * @test
     * @dataProvider invalidParamsDataProvider
     */
    public function givenAnInvalidParamsThenItThrowsAnException(
        $sellerId,
        $productId,
        $quantity,
        $expectedExceptionMessage
    ) {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($expectedExceptionMessage);
        new DecreaseQuantityCommand($productId, $sellerId, $quantity);
    }

    /** @test */
    public function givenAInvalidProductThenThrowsAnException()
    {
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $stockRepository
            ->method('getStockByProductAndSellerId')
            ->willReturn(null);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Product not found');

        $handler = new DecreaseQuantityCommandHandler($stockRepository);
        $handler(new DecreaseQuantityCommand('the-id', 'the-seller', 1));
    }

    /** @test */
    public function givenAValidProductThenIncreasesTheQuantity()
    {
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $stock = new Stock(
            new Product('the-id', 'Product name'),
            new Seller('the-seller', 'Third seller'),
            11,
            19.95
        );
        $stockRepository
            ->method('getStockByProductAndSellerId')
            ->willReturn($stock);
        $stockRepository
            ->expects($this->once())
            ->method('updateStock');

        $handler = new DecreaseQuantityCommandHandler($stockRepository);
        $handler(new DecreaseQuantityCommand('the-id', 'the-seller', 1));
    }

    public function invalidParamsDataProvider()
    {
        return[
            [1, 'product-id', 10, 'Invalid seller id'],
            ['', 'product-id', 10, 'Invalid seller id'],
            [null, 'product-id', 10, 'Invalid seller id'],
            ['seller-id', 1,  10, 'Invalid product id'],
            ['seller-id', '',  10, 'Invalid product id'],
            ['seller-id', null, 10, 'Invalid product id'],
            ['seller-id', 'product-id', null, 'Invalid product quantity'],
            ['seller-id', 'product-id', 'price', 'Invalid product quantity']
        ];
    }
}
