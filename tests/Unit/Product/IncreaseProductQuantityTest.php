<?php

namespace Unit\Product;

use App\ShoppingCart\Product\Domain\Service\IncreaseQuantity\IncreaseQuantityCommand;
use App\ShoppingCart\Product\Domain\Service\IncreaseQuantity\IncreaseQuantityCommandHandler;
use App\ShoppingCart\Product\Domain\ValueObject\Product;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;
use App\ShoppingCart\Shared\Domain\ValueObject\Stock;
use PHPUnit\Framework\TestCase;

class IncreaseProductQuantityTest extends TestCase
{
    /** @test */
    public function givenAInvalidProductThenThrowsAnException()
    {
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $stockRepository
            ->method('getStockByProductAndSellerId')
            ->willReturn(null);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Product not found');

        $handler = new IncreaseQuantityCommandHandler($stockRepository);
        $handler(new IncreaseQuantityCommand('the-id', 'the-seller'));
    }
    /** @test */
    public function givenAValidProductThenIncreasesTheQuantity()
    {
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $stock = new Stock(
            new Product('the-id', 'Product name'),
            new Seller('the-seller', 'Third seller'),
            11,
            19.95
        );
        $stockRepository
            ->method('getStockByProductAndSellerId')
            ->willReturn($stock);
        $stockRepository
            ->expects($this->once())
            ->method('updateStock');

        $handler = new IncreaseQuantityCommandHandler($stockRepository);
        $handler(new IncreaseQuantityCommand('the-id', 'the-seller'));
    }
}
