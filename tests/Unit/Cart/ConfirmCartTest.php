<?php

namespace Unit\Cart;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Cart\Domain\Service\ConfirmCart\ConfirmCartCommandHandler;
use App\ShoppingCart\Cart\Domain\ValueObject\Cart;
use App\ShoppingCart\Cart\Domain\ValueObject\CartLine;
use App\ShoppingCart\Product\Domain\Service\DecreaseQuantity\DecreaseQuantityCommand;
use App\ShoppingCart\Product\Domain\Service\DecreaseQuantity\DecreaseQuantityCommandHandler;
use App\ShoppingCart\Product\Domain\ValueObject\Product;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;
use App\ShoppingCart\Shared\Domain\ValueObject\Stock;
use PHPUnit\Framework\TestCase;

class ConfirmCartTest extends TestCase
{
    /** @test */
    public function ifCartIsEmptyThenItCanNotBeConfirmed()
    {
        $cartRepository = $this->getMockBuilder(CartRepository::class)->getMock();
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $decreaseQuantityCommandHandler = new DecreaseQuantityCommandHandler($stockRepository);
        $handler = new ConfirmCartCommandHandler($cartRepository, $stockRepository, $decreaseQuantityCommandHandler);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Empty cart cannot be confirmed');
        $handler();
    }

    /** @test */
    public function ifCartIsNotEmptyThenItCanBeConfirmed()
    {
        $cartRepository = $this->getMockBuilder(CartRepository::class)->getMock();
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $cartRepository
            ->method('getCart')
            ->willReturn(
                new Cart([
                    new CartLine(
                        new Stock(
                            new Product('the-id', 'The product'),
                            new Seller('the-seller', 'The seller'),
                            10,
                            0.99
                        ),
                        1
                    )
                ])
            );
        $decreaseQuantityCommandHandler = $this->getMockBuilder(DecreaseQuantityCommandHandler::class)
            ->disableOriginalConstructor()
            ->getMock();

        $decreaseQuantityCommandHandler
            ->expects($this->once())
            ->method('__invoke')
            ->with(new DecreaseQuantityCommand('the-id', 'the-seller', 1));
        $handler = new ConfirmCartCommandHandler($cartRepository, $stockRepository, $decreaseQuantityCommandHandler);
        $handler();
    }
}
