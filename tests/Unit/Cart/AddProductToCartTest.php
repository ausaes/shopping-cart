<?php

namespace Unit\Cart;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Cart\Domain\Service\AddProductToCart\AddProductToCartCommand;
use App\ShoppingCart\Cart\Domain\Service\AddProductToCart\AddProductToCartCommandHandler;
use App\ShoppingCart\Product\Domain\ValueObject\Product;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;
use App\ShoppingCart\Shared\Domain\ValueObject\Stock;
use PHPUnit\Framework\TestCase;

class AddProductToCartTest extends TestCase
{
    /** @test */
    public function givenAValidProductThenAddToCart()
    {
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $cartRepository = $this->getMockBuilder(CartRepository::class)->getMock();

        $stockRepository
            ->method('getCheapestStockFromProductId')
            ->willReturn($this->getStock());

        $cartRepository
            ->expects($this->once())
            ->method('addToCart');
        $handler = new AddProductToCartCommandHandler($stockRepository, $cartRepository);
        $handler(new AddProductToCartCommand('product-id'));
    }

    /** @test */
    public function givenAInvalidProductThenThrowsAnException()
    {
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $cartRepository = $this->getMockBuilder(CartRepository::class)->getMock();

        $stockRepository
            ->method('getCheapestStockFromProductId')
            ->willReturn(null);

        $handler = new AddProductToCartCommandHandler($stockRepository, $cartRepository);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Product not found');
        $handler(new AddProductToCartCommand('product-id'));
    }

    private function getStock(): Stock
    {
        return new Stock(
            new Product('product-id', 'Product name'),
            new Seller('seller-id', 'Seller name'),
            100,
            0.99
        );
    }

}
