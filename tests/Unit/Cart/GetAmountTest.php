<?php

namespace Unit\Cart;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Cart\Domain\Service\GetTotalAmount\GetTotalAmountQuery;
use App\ShoppingCart\Cart\Domain\Service\GetTotalAmount\GetTotalAmountQueryHandler;
use App\ShoppingCart\Cart\Domain\Service\GetTotalAmount\GetTotalAmountResponse;
use App\ShoppingCart\Cart\Domain\ValueObject\Cart;
use App\ShoppingCart\Cart\Domain\ValueObject\CartLine;
use App\ShoppingCart\Product\Domain\ValueObject\Product;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;
use App\ShoppingCart\Shared\Domain\ValueObject\Stock;
use PHPUnit\Framework\TestCase;

class GetAmountTest extends TestCase
{
    /** @test */
    public function givenAEmptyCartThenItReturnsZeroAmount()
    {
        $handler = new GetTotalAmountQueryHandler($this->getCartRepository());
        /** @var GetTotalAmountResponse $response */
        $response = $handler(new GetTotalAmountQuery());

        $this->assertEquals(0.0, $response->getAmount());
    }

    /** @test */
    public function givenACartWithARepeteatedProductThenReturnsTheCorrectPrice()
    {
        $cartLines = [
            $this->getCartLine(10.0, 2)
        ];
        $handler = new GetTotalAmountQueryHandler($this->getCartRepository($cartLines));
        /** @var GetTotalAmountResponse $response */
        $response = $handler(new GetTotalAmountQuery());

        $this->assertEquals(20.0, $response->getAmount());
    }

    /** @test */
    public function givenACartWithAProductThenReturnsTheCorrectPrice()
    {
        $cartLines = [
            $this->getCartLine(10.0, 1)
        ];
        $handler = new GetTotalAmountQueryHandler($this->getCartRepository($cartLines));
        /** @var GetTotalAmountResponse $response */
        $response = $handler(new GetTotalAmountQuery());

        $this->assertEquals(10.0, $response->getAmount());
    }

    /** @test */
    public function givenACartWithMultipleProductsThenReturnsTheCorrectPrice()
    {
        $cartLines = [
            $this->getCartLine(10.0, 1),
            $this->getCartLine(15.0, 1)
        ];
        $handler = new GetTotalAmountQueryHandler($this->getCartRepository($cartLines));
        /** @var GetTotalAmountResponse $response */
        $response = $handler(new GetTotalAmountQuery());

        $this->assertEquals(25.0, $response->getAmount());
    }

    private function getCartRepository(array $cartLines = []): CartRepository
    {
        $cartRepository = $this->getMockBuilder(CartRepository::class)->getMock();
        $cartRepository
            ->method('getCart')
            ->willReturn(
                new Cart($cartLines)
            );
        return $cartRepository;
    }
    private function getCartLine(float $price, int $quantity): CartLine
    {
        return new CartLine(
            new Stock(
                new Product('product-id', 'Product name'),
                new Seller('seller-id', 'Seller name'),
                100,
                $price
            ),
            $quantity
        );
    }
}
