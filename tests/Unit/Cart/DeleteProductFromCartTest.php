<?php

namespace Unit\Cart;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Cart\Domain\Service\DeleteProductFromCart\DeleteProductFromCartCommand;
use App\ShoppingCart\Cart\Domain\Service\DeleteProductFromCart\DeleteProductFromCartCommandHandler;
use PHPUnit\Framework\TestCase;

class DeleteProductFromCartTest extends TestCase
{
    /** @test */
    public function givenAValidProductThenDeleteFromCart()
    {
        $cartRepository = $this->getMockBuilder(CartRepository::class)->getMock();
        $cartRepository
            ->expects($this->once())
            ->method('deleteFromCart');
        $handler = new DeleteProductFromCartCommandHandler($cartRepository);
        $handler(new DeleteProductFromCartCommand('product-id'));
    }

}
