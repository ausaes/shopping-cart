<?php

namespace Unit\Cart;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Cart\Domain\Service\EmptyCart\EmptyCartCommandHandler;
use PHPUnit\Framework\TestCase;

class EmptyCartTest extends TestCase
{
    /** @test */
    public function itShouldEmptyTheCart()
    {
        $repository = $this->getMockBuilder(CartRepository::class)->getMock();

        $repository
            ->expects($this->once())
            ->method('emptyCart');
        $handler = new EmptyCartCommandHandler($repository);
        $handler();
    }
}
