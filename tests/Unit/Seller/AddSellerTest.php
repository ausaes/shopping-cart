<?php

namespace Unit\Seller;

use App\ShoppingCart\Seller\Domain\Repository\SellerRepository;
use App\ShoppingCart\Seller\Domain\Service\AddSeller\AddSellerCommand;
use App\ShoppingCart\Seller\Domain\Service\AddSeller\AddSellerCommandHandler;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;
use PHPUnit\Framework\TestCase;

class AddSellerTest extends TestCase
{
    /**
     * @test
     * @dataProvider InvalidParamsDataProvider
     */
    public function givenAnInvalidParamsThenItThrowsAnError(
        $id, $name, $expectedId
    ) {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($expectedId);
        new AddSellerCommand($id, $name);

    }

    /** @test */
    public function givenAValidInputThenItCreatesTheSeller()
    {
        $sellerRepository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $sellerRepository
            ->expects($this->once())
            ->method('addSeller');
        $handler = new AddSellerCommandHandler($sellerRepository);
        $handler(new AddSellerCommand('the-id', 'The name'));
    }

    /** @test */
    public function givenAnAlreadyCreatedSellerThenTheServiceThrowsAnException()
    {
        $sellerRepository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $sellerRepository
            ->method('getSeller')
            ->willReturn(new Seller('the-id', 'The name'));
        $sellerRepository
            ->expects($this->never())
            ->method('addSeller');

        $handler = new AddSellerCommandHandler($sellerRepository);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Seller already exists');
        $handler(new AddSellerCommand('the-id', 'The name'));
    }

    public function InvalidParamsDataProvider()
    {
        return
            [
                [1, 'name', 'Invalid Id'],
                ['', 'name', 'Invalid Id'],
                [null, 'name', 'Invalid Id'],
                [null, null, 'Invalid Id'],
                 ['id', 1, 'Invalid name'],
                ['id', '', 'Invalid name'],
                ['id', null, 'Invalid name'],
            ];
    }
}
