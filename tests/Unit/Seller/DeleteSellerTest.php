<?php

namespace Unit\Seller;

use App\ShoppingCart\Seller\Domain\Repository\SellerRepository;
use App\ShoppingCart\Seller\Domain\Service\DeleteSeller\DeleteSellerCommand;
use App\ShoppingCart\Seller\Domain\Service\DeleteSeller\DeleteSellerCommandHandler;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;
use PHPUnit\Framework\TestCase;

class DeleteSellerTest extends TestCase
{
    /**
     * @test
     * @dataProvider InvalidParamsDataProvider
     */
    public function givenAnInvalidParamsThenItThrowsAnError(
        $id,
        $expectedExceptionMessage
    )
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($expectedExceptionMessage);
        new DeleteSellerCommand($id);
    }

    /** @test */
    public function givenAValidIdThenItDeletesTheSeller()
    {
        $repository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $repository
            ->method('getSeller')
            ->willReturn(new Seller('the-id', 'The name'));

        $repository
            ->expects($this->once())
            ->method('deleteSeller');
        $handler = new DeleteSellerCommandHandler($repository);
        $handler(new DeleteSellerCommand('the-id'));
    }

    /** @test */
    public function givenAInvalidIdThenItThrowsAnException()
    {
        $repository = $this->getMockBuilder(SellerRepository::class)->getMock();
        $repository
            ->method('getSeller')
            ->willReturn(null);


        $handler = new DeleteSellerCommandHandler($repository);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Seller not found');
        $handler(new DeleteSellerCommand('the-id'));
    }

    public function InvalidParamsDataProvider()
    {
        return [
            [1, 'Invalid Id'],
            ['', 'Invalid Id'],
            [null, 'Invalid Id'],
        ];
    }
}
