# Shopping cart
## How to init this project

### Add host to your local /etc/hosts
```
127.0.0.1   www.shopping-cart.local
```

## Build Project
```
docker-compose up
```

### Install dependencies
```
docker exec -it shopping-cart.php composer install
```

### Run tests
```
docker exec -it shopping-cart.php php vendor/bin/phpunit
```

### Run behat tests
```
docker exec -it shopping-cart.php php vendor/bin/behat
```

### Play with the API
You can:
- add seller
``
POST /seller
``
- delete seller
``
DELETE /seller
``
- add product ``POST /seller/{sellerId}/product/{productId}``
- delete product ``DELETE /seller/{sellerId}/product/{productId}``
- increase product quantity ``PATCH /seller/{sellerId}/product/{productId}/increase``
- decrease product quantity ``PATCH /seller/{sellerId}/product/{productId}/decrease``
- add product to cart ``POST /cart/{productId}``
- delete product from cart ``DELETE /cart/{productId}``
- empty cart ``DELETE /cart``
- confirm cart ``POST /cart/confirm``
- get total amount ``GET /cart/amount``