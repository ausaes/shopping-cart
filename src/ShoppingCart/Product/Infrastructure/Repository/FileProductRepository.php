<?php

namespace App\ShoppingCart\Product\Infrastructure\Repository;

use App\ShoppingCart\Product\Domain\Repository\ProductRepository;
use App\ShoppingCart\Product\Domain\ValueObject\Product;

class FileProductRepository implements ProductRepository
{
    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function getProduct(string $id): ?Product
    {
        return $this->getProducts()[$id] ?? null;
    }

    /**
     * @return Product[]
     */
    private function getProducts(): array
    {
        $products = [];
        $file = file_get_contents($this->filename);
        foreach(json_decode($file, true) as $rawProduct) {
            $products[$rawProduct['id']] = new Product($rawProduct['id'], $rawProduct['name']);
        }
        return $products;
    }

}
