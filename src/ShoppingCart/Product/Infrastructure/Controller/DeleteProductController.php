<?php

namespace App\ShoppingCart\Product\Infrastructure\Controller;

use App\ShoppingCart\Product\Domain\Service\DeleteProduct\DeleteProductCommand;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DeleteProductController
{
    private CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(Request $request)
    {
        try {
            $command = new DeleteProductCommand($request->get('sellerId'), $request->get('productId'));
            $this->commandBus->dispatch($command);
            return new JsonResponse(null, 201);
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'info' => $exception->getMessage()], 400);
        }
    }
}
