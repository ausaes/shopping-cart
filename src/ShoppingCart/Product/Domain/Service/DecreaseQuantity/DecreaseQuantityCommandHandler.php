<?php

namespace App\ShoppingCart\Product\Domain\Service\DecreaseQuantity;

use App\ShoppingCart\Shared\Domain\Bus\Command\CommandHandler;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;
use App\ShoppingCart\Shared\Domain\ValueObject\Stock;

class DecreaseQuantityCommandHandler implements CommandHandler
{
    private StockRepository $stockRepository;

    public function __construct(
        StockRepository $stockRepository
    ) {
        $this->stockRepository = $stockRepository;
    }

    public function __invoke(DecreaseQuantityCommand $command)
    {
        $stock = $this->stockRepository->getStockByProductAndSellerId($command->getProductId(), $command->getSellerId());
        if (!$stock) {
            throw new \Exception('Product not found');
        }
        if ($stock->getQuantity() > 0)
        {
            $this->stockRepository->updateStock(
                new Stock(
                    $stock->getProduct(),
                    $stock->getSeller(),
                    $stock->getQuantity() - $command->getQuantity(),
                    $stock->getPrice())
            );
        }
    }

}
