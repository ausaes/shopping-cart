<?php

namespace App\ShoppingCart\Product\Domain\Service\DecreaseQuantity;

use App\ShoppingCart\Shared\Domain\Bus\Command\Command;

class DecreaseQuantityCommand implements Command
{
    private string $productId;
    private string $sellerId;
    private int $quantity;


    public function __construct($productId, $sellerId, $quantity)
    {
        $this->setProductId($productId);
        $this->setSellerId($sellerId);
        $this->setQuantity($quantity);
    }

    public function setProductId($productId): void
    {
        if (empty($productId) || !is_string($productId)) {
            throw new \Exception('Invalid product id');
        }
        $this->productId = $productId;
    }

    public function setSellerId($sellerId): void
    {
        if (empty($sellerId) || !is_string($sellerId)) {
            throw new \Exception('Invalid seller id');
        }
        $this->sellerId = $sellerId;
    }

    public function setQuantity($quantity): void
    {
        if (empty($quantity) || !is_numeric($quantity)) {
            throw new \Exception('Invalid product quantity');
        }
        $this->quantity = (int)$quantity;
    }


    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getSellerId(): string
    {
        return $this->sellerId;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
