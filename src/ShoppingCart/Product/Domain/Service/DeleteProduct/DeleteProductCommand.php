<?php

namespace App\ShoppingCart\Product\Domain\Service\DeleteProduct;

use App\ShoppingCart\Shared\Domain\Bus\Command\Command;

class DeleteProductCommand implements Command
{
    private string $sellerId;
    private string $productId;

    public function __construct($sellerId, $productId)
    {
        $this->setSellerId($sellerId);
        $this->setProductId($productId);
    }

    public function getSellerId(): string
    {
        return $this->sellerId;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setSellerId($sellerId): void
    {
        if (empty($sellerId) || !is_string($sellerId)) {
            throw new \Exception('Invalid seller id');
        }
        $this->sellerId = $sellerId;
    }

    public function setProductId($productId): void
    {
        if (empty($productId) || !is_string($productId)) {
            throw new \Exception('Invalid product id');
        }
        $this->productId = $productId;
    }

}
