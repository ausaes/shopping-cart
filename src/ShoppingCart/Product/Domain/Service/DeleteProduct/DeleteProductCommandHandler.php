<?php

namespace App\ShoppingCart\Product\Domain\Service\DeleteProduct;

use App\ShoppingCart\Product\Domain\Repository\ProductRepository;
use App\ShoppingCart\Seller\Domain\Repository\SellerRepository;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandHandler;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;

class DeleteProductCommandHandler implements CommandHandler
{
    private StockRepository $sellerProductRepository;
    private ProductRepository $productRepository;
    private SellerRepository $sellerRepository;

    public function __construct(
        ProductRepository $productRepository,
        SellerRepository $sellerRepository,
        StockRepository $sellerProductRepository
    ) {
        $this->sellerProductRepository = $sellerProductRepository;
        $this->productRepository = $productRepository;
        $this->sellerRepository = $sellerRepository;
    }

    public function __invoke(DeleteProductCommand $command)
    {
        if (!$this->sellerRepository->getSeller($command->getSellerId())) {
            throw new \Exception('Seller not found');
        }
        if (!$this->productRepository->getProduct($command->getProductId())) {
            throw new \Exception('Product not found');
        }
        $this->sellerProductRepository->unlinkProductToSeller($command->getProductId(), $command->getSellerId());
    }

}
