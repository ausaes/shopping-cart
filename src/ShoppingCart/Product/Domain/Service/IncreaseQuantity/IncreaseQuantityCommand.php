<?php

namespace App\ShoppingCart\Product\Domain\Service\IncreaseQuantity;

use App\ShoppingCart\Shared\Domain\Bus\Command\Command;

class IncreaseQuantityCommand implements Command
{
    private string $productId;
    private string $sellerId;


    public function __construct(string $productId, string $sellerId)
    {
        $this->productId = $productId;
        $this->sellerId = $sellerId;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getSellerId(): string
    {
        return $this->sellerId;
    }


}
