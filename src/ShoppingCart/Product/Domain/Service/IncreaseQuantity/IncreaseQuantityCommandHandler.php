<?php

namespace App\ShoppingCart\Product\Domain\Service\IncreaseQuantity;

use App\ShoppingCart\Shared\Domain\Bus\Command\CommandHandler;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;
use App\ShoppingCart\Shared\Domain\ValueObject\Stock;

class IncreaseQuantityCommandHandler implements CommandHandler
{
    private StockRepository $stockRepository;

    public function __construct(
        StockRepository $stockRepository
    ) {
        $this->stockRepository = $stockRepository;
    }

    public function __invoke(IncreaseQuantityCommand $command)
    {
        $stock = $this->stockRepository->getStockByProductAndSellerId($command->getProductId(), $command->getSellerId());
        if (!$stock) {
            throw new \Exception('Product not found');
        }
        $this->stockRepository->updateStock(
            new Stock(
                $stock->getProduct(),
                $stock->getSeller(),
                $stock->getQuantity() + 1,
                $stock->getPrice())
        );
    }

}
