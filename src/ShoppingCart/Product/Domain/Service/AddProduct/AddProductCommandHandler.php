<?php

namespace App\ShoppingCart\Product\Domain\Service\AddProduct;

use App\ShoppingCart\Product\Domain\Repository\ProductRepository;
use App\ShoppingCart\Seller\Domain\Repository\SellerRepository;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandHandler;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;

class AddProductCommandHandler implements CommandHandler
{
    private ProductRepository $productRepository;
    private StockRepository $sellerProductRepository;
    private SellerRepository $sellerRepository;

    public function __construct(
        ProductRepository $productRepository,
        StockRepository $sellerProductRepository,
        SellerRepository $sellerRepository
    ) {

        $this->productRepository = $productRepository;
        $this->sellerProductRepository = $sellerProductRepository;
        $this->sellerRepository = $sellerRepository;
    }

    public function __invoke(AddProductCommand $command)
    {
        if (!$this->sellerRepository->getSeller($command->getSellerId())) {
            throw new \Exception('Seller not found');
        }
        if (!$this->productRepository->getProduct($command->getProductId())) {
            throw new \Exception('Product not found');
        }
        $this->sellerProductRepository->linkProductToSeller(
            $command->getProductId(),
            $command->getSellerId(),
            $command->getProductPrice()
        );
    }
}
