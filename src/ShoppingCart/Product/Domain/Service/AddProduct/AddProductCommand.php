<?php

namespace App\ShoppingCart\Product\Domain\Service\AddProduct;

use App\ShoppingCart\Shared\Domain\Bus\Command\Command;

class AddProductCommand implements Command
{
    private string $sellerId;
    private string $productId;
    private string $productPrice;

    public function __construct($sellerId, $productId, $productPrice)
    {
        $this->setSellerId($sellerId);
        $this->setProductId($productId);
        $this->setProductPrice($productPrice);
    }

    public function setSellerId($sellerId): void
    {
        if (empty($sellerId) || !is_string($sellerId)) {
            throw new \Exception('Invalid seller id');
        }
        $this->sellerId = $sellerId;
    }


    public function setProductId($productId): void
    {
        if (empty($productId) || !is_string($productId)) {
            throw new \Exception('Invalid product id');
        }
        $this->productId = $productId;
    }

    public function setProductPrice($productPrice): void
    {
        if (empty($productPrice) || !is_numeric($productPrice)) {
            throw new \Exception('Invalid product price');
        }
        $this->productPrice = (float)$productPrice;
    }


    public function getSellerId(): string
    {
        return $this->sellerId;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getProductPrice(): string
    {
        return $this->productPrice;
    }


}
