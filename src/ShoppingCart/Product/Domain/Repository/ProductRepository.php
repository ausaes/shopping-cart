<?php

namespace App\ShoppingCart\Product\Domain\Repository;

use App\ShoppingCart\Product\Domain\ValueObject\Product;

interface ProductRepository
{
    public function getProduct(string $id): ?Product;
}
