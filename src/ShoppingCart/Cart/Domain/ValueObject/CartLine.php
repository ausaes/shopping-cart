<?php

namespace App\ShoppingCart\Cart\Domain\ValueObject;

use App\ShoppingCart\Shared\Domain\ValueObject\Stock;

class CartLine
{
    private Stock $stock;
    private int $quantity;

    public function __construct(Stock $stock, int $quantity)
    {
        $this->stock = $stock;
        $this->quantity = $quantity;
    }

    public function getStock(): Stock
    {
        return $this->stock;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

}
