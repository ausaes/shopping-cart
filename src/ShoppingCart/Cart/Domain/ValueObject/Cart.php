<?php

namespace App\ShoppingCart\Cart\Domain\ValueObject;

class Cart
{
    /** @var CartLine[]  */
    private array $lines;

    /**
     * @param CartLine[] $lines
     */
    public function __construct($lines)
    {
        $this->lines = $lines;
    }

    /**
     * @return CartLine[]
     */
    public function getLines(): array
    {
        return $this->lines;
    }

    public function getTotalAmount(): float
    {
        $amount = 0.0;
        foreach($this->getLines() as $cartLine) {
            $amount += $this->getLineAmount($cartLine);
        }
        return $amount;
    }

    private function getLineAmount(CartLine $cartLine): float
    {
        return $cartLine->getStock()->getPrice() * $cartLine->getQuantity();
    }
}
