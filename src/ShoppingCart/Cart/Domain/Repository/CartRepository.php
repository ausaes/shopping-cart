<?php

namespace App\ShoppingCart\Cart\Domain\Repository;

use App\ShoppingCart\Cart\Domain\ValueObject\Cart;
use App\ShoppingCart\Shared\Domain\ValueObject\Stock;

interface CartRepository
{
    public function getCart(): Cart;

    public function addToCart(Stock $stock): void;

    public function deleteFromCart(string $productId): void;

    public function emptyCart(): void;
}
