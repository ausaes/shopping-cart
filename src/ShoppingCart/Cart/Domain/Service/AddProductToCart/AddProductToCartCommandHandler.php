<?php

namespace App\ShoppingCart\Cart\Domain\Service\AddProductToCart;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;

class AddProductToCartCommandHandler
{
    private CartRepository $cartRepository;
    private StockRepository $stockRepository;

    public function __construct(
        StockRepository $stockRepository,
        CartRepository $cartRepository
    ) {
        $this->cartRepository = $cartRepository;
        $this->stockRepository = $stockRepository;
    }

    public function __invoke(AddProductToCartCommand $command)
    {
        $stock = $this->stockRepository->getCheapestStockFromProductId($command->getProductId());
        if (!$stock) {
            throw new \Exception('Product not found');
        }
        $this->cartRepository->addToCart($stock);
    }

}
