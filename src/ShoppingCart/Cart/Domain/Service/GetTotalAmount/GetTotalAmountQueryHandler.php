<?php

namespace App\ShoppingCart\Cart\Domain\Service\GetTotalAmount;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Shared\Domain\Bus\Query\QueryHandler;
use App\ShoppingCart\Shared\Domain\Bus\Query\Response;

class GetTotalAmountQueryHandler implements QueryHandler
{
    private CartRepository $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function __invoke(GetTotalAmountQuery $query): Response
    {
        $cart = $this->cartRepository->getCart();
        return new GetTotalAmountResponse($cart->getTotalAmount());
    }
}
