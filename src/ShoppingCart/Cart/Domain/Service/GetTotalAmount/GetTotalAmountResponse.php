<?php

namespace App\ShoppingCart\Cart\Domain\Service\GetTotalAmount;

use App\ShoppingCart\Shared\Domain\Bus\Query\Response;

class GetTotalAmountResponse implements Response
{
    private float $amount;

    public function __construct(float $amount)
    {
        $this->amount = $amount;
    }
    public function getAmount(): float
    {
        return $this->amount;
    }
}
