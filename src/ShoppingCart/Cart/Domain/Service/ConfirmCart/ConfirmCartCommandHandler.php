<?php

namespace App\ShoppingCart\Cart\Domain\Service\ConfirmCart;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Cart\Domain\ValueObject\Cart;
use App\ShoppingCart\Product\Domain\Service\DecreaseQuantity\DecreaseQuantityCommand;
use App\ShoppingCart\Product\Domain\Service\DecreaseQuantity\DecreaseQuantityCommandHandler;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandHandler;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;

class ConfirmCartCommandHandler implements CommandHandler
{
    private CartRepository $cartRepository;
    private StockRepository $stockRepository;
    private DecreaseQuantityCommandHandler $decreaseQuantityCommandHandler;

    public function __construct(
        CartRepository $cartRepository,
        StockRepository $stockRepository,
        DecreaseQuantityCommandHandler $decreaseQuantityCommandHandler
    ) {
        $this->cartRepository = $cartRepository;
        $this->stockRepository = $stockRepository;
        $this->decreaseQuantityCommandHandler = $decreaseQuantityCommandHandler;
    }

    public function __invoke()
    {
        $cart = $this->cartRepository->getCart();
        if (count($cart->getLines()) === 0) {
            throw new \Exception('Empty cart cannot be confirmed');
        }
        $this->manageStock($cart);
        $this->cartRepository->emptyCart();
    }

    private function manageStock(Cart $cart)
    {
        foreach ($cart->getLines() as $cartLine) {
            $this->decreaseQuantityCommandHandler->__invoke(
                new DecreaseQuantityCommand(
                    $cartLine->getStock()->getProduct()->getId(),
                    $cartLine->getStock()->getSeller()->getId(),
                    $cartLine->getQuantity()
                )
            );
        }
    }
}
