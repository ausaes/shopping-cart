<?php

namespace App\ShoppingCart\Cart\Domain\Service\DeleteProductFromCart;

use App\ShoppingCart\Shared\Domain\Bus\Command\Command;

class DeleteProductFromCartCommand implements Command
{
    private string $productId;

    public function __construct(string $productId)
    {
        $this->productId = $productId;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }
}
