<?php

namespace App\ShoppingCart\Cart\Domain\Service\DeleteProductFromCart;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandHandler;

class DeleteProductFromCartCommandHandler implements CommandHandler
{
    private CartRepository $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function __invoke(DeleteProductFromCartCommand $command)
    {
        $this->cartRepository->deleteFromCart($command->getProductId());
    }
}
