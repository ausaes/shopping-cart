<?php

namespace App\ShoppingCart\Cart\Domain\Service\EmptyCart;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandHandler;

class EmptyCartCommandHandler implements CommandHandler
{

    private CartRepository $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function __invoke()
    {
        $this->cartRepository->emptyCart();
    }
}
