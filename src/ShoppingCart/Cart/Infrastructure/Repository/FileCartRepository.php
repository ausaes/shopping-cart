<?php

namespace App\ShoppingCart\Cart\Infrastructure\Repository;

use App\ShoppingCart\Cart\Domain\Repository\CartRepository;
use App\ShoppingCart\Cart\Domain\ValueObject\Cart;
use App\ShoppingCart\Cart\Domain\ValueObject\CartLine;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;
use App\ShoppingCart\Shared\Domain\ValueObject\Stock;

class FileCartRepository implements CartRepository
{
    private string $filename;

    private StockRepository $stockRepository;

    public function __construct(
        string $filename,
        StockRepository $stockRepository
    ) {
        $this->filename = $filename;
        $this->stockRepository = $stockRepository;
    }

    public function addToCart(Stock $stock): void
    {
        if ($this->stockIsAlreadyInLines($stock)) {
            $this->changeQuantityToCartProduct($stock, 1);
        } else {
            $lines = $this->getCart()->getLines();
            $lines[] = new CartLine($stock, 1);
            $this->setCartToFile(new Cart($lines));
        }
    }

    public function deleteFromCart(string $productId): void
    {
        $cartLine = $this->getCartLineFromId($productId);
        if ($cartLine->getQuantity() > 1) {
            $this->changeQuantityToCartProduct($cartLine->getStock(), -1);
        } else {
            $this->removeStockFromCart($cartLine->getStock());
        }
    }
    public function emptyCart(): void
    {
        $this->setCartToFile(new Cart([]));
    }

    public function getCart(): Cart
    {
        return $this->getCartFromFile();
    }

    private function getCartFromFile(): Cart
    {
        $file = file_get_contents($this->filename);
        $lines = [];
        foreach (json_decode($file, true) as $rawFile) {
            $lines[] = new CartLine(
                $this->getStock($rawFile['sellerId'], $rawFile['productId']),
                $rawFile['quantity']
            );
        }
        return new Cart($lines);
    }

    private function setCartToFile(Cart $cart): void
    {
        $rawCart = [];
        foreach ($cart->getLines() as $cartLine) {
            $rawCart[] = [
                'sellerId' => $cartLine->getStock()->getSeller()->getId(),
                'productId' => $cartLine->getStock()->getProduct()->getId(),
                'quantity' => $cartLine->getQuantity()
            ];
        }
        $file = fopen($this->filename, 'w');
        fwrite($file, json_encode($rawCart));
    }

    private function getStock($sellerId, $productId): Stock
    {
        return $this->stockRepository->getStockByProductAndSellerId($productId, $sellerId);
    }

    private function stockIsAlreadyInLines(Stock $stock): bool
    {
        foreach ($this->getCart()->getLines() as $line) {
            if ($stock->isEqualsTo($line->getStock())) {
                return true;
            }
        }
        return false;
    }

    private function changeQuantityToCartProduct(Stock $stock, int $quantity)
    {
        $newLines = [];
        foreach ($this->getCart()->getLines() as $line) {
            if ($stock->isEqualsTo($line->getStock())) {
                $newLines[] = new CartLine($line->getStock(), $line->getQuantity() + $quantity);
            } else {
                $newLines[] = $line;
            }
        }
        $this->setCartToFile(new Cart($newLines));
    }

    private function getCartLineFromId(string $id): CartLine
    {
        foreach ($this->getCartFromFile()->getLines() as $cartLine) {
            if ($cartLine->getStock()->getProduct()->getId() === $id) {
                return $cartLine;
            }
        }
        throw new \Exception('Stock not found');
    }

    private function removeStockFromCart(Stock $stock): void
    {
        $lines = [];
        foreach ($this->getCartFromFile()->getLines() as $cartLine) {
            if (!$stock->isEqualsTo($cartLine->getStock())) {
                $lines[] = $cartLine;
            }
        }
        $this->setCartToFile(new Cart($lines));
    }
}
