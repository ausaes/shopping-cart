<?php

namespace App\ShoppingCart\Cart\Infrastructure\Controller;

use App\ShoppingCart\Cart\Domain\Service\GetTotalAmount\GetTotalAmountQuery;
use App\ShoppingCart\Cart\Domain\Service\GetTotalAmount\GetTotalAmountResponse;
use App\ShoppingCart\Shared\Domain\Bus\Query\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GetCartAmountController
{
    private QueryBus $queryBus;

    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }
    public function __invoke(Request $request)
    {
        /** @var GetTotalAmountResponse $response */
        $response = $this->queryBus->ask(new GetTotalAmountQuery());
        return new JsonResponse(['amount' => $response->getAmount()], 200);
    }
}
