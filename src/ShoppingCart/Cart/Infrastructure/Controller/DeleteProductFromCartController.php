<?php

namespace App\ShoppingCart\Cart\Infrastructure\Controller;

use App\ShoppingCart\Cart\Domain\Service\DeleteProductFromCart\DeleteProductFromCartCommand;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DeleteProductFromCartController
{
    private CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(Request $request)
    {
        try {
            $command = new DeleteProductFromCartCommand($request->get('productId'));
            $this->commandBus->dispatch($command);
            return new JsonResponse(null, 202);
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'info' => $exception->getMessage()], 400);
        }
    }
}
