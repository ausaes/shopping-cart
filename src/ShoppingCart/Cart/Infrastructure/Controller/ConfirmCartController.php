<?php

namespace App\ShoppingCart\Cart\Infrastructure\Controller;

use App\ShoppingCart\Cart\Domain\Service\ConfirmCart\ConfirmCartCommand;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ConfirmCartController
{
    private CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(Request $request)
    {
        try {
            $command = new ConfirmCartCommand();
            $this->commandBus->dispatch($command);
            return new JsonResponse(null, 201);
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'info' => $exception->getMessage()], 400);
        }
    }
}
