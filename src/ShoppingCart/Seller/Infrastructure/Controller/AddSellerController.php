<?php

namespace App\ShoppingCart\Seller\Infrastructure\Controller;

use App\ShoppingCart\Seller\Domain\Service\AddSeller\AddSellerCommand;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AddSellerController
{
    private CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(Request $request)
    {
        try {
            $command = new AddSellerCommand($request->request->get('id'), $request->get('name'));
            $this->commandBus->dispatch($command);
            return new JsonResponse(null, 201);
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'info' => $exception->getMessage()], 400);
        }
    }
}
