<?php

namespace App\ShoppingCart\Seller\Infrastructure\Controller;

use App\ShoppingCart\Seller\Domain\Service\DeleteSeller\DeleteSellerCommand;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DeleteSellerController
{
    private CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }
    public function __invoke(Request $request)
    {
        try {
            $command = new DeleteSellerCommand($request->get('id'));
            $this->commandBus->dispatch($command);
            return new JsonResponse(null, 202);
        } catch (\Exception $exception) {
            return new JsonResponse(['status' => 'error', 'info' => $exception->getMessage()], 400);
        }
    }
}
