<?php

namespace App\ShoppingCart\Seller\Infrastructure\Repository;

use App\ShoppingCart\Seller\Domain\Repository\SellerRepository;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;

class FileSellerRepository implements SellerRepository
{
    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function addSeller(Seller $seller): void
    {
        if(!empty($this->getSeller($seller->getId())))
        {
            throw new \Exception('The seller already exists');
        }
        $sellers = $this->getSellers();

        $sellers[$seller->getId()] = $seller;
        $this->setSellers($sellers);

    }

    public function getSeller(string $id): ?Seller
    {
        $sellers = $this->getSellers();
        return $sellers[$id] ?? null;
    }


    public function deleteSeller(string $id): void
    {
        if(empty($this->getSeller($id)))
        {
            throw new \Exception('Seller not found');
        }
        $sellers = $this->getSellers();
        unset($sellers[$id]);
        $this->setSellers($sellers);
    }

    /**
     * @return Seller[]
     */
    private function getSellers(): array
    {
        $sellers = [];
        $file = file_get_contents($this->filename);
        foreach(json_decode($file, true) as $rawSeller)
        {
            $sellers[$rawSeller['id']] = new Seller($rawSeller['id'], $rawSeller['name']);
        }
        return $sellers;
    }
    private function setSellers(array $sellers)
    {
        $rawSellers = [];
        foreach($sellers as $seller){
            $rawSellers[] = ['id' =>$seller->getId(), 'name' => $seller->getName()];
        }
        $file = fopen($this->filename, 'w');
        fwrite($file, json_encode($rawSellers));
    }

}
