<?php

namespace App\ShoppingCart\Seller\Domain\Repository;

use App\ShoppingCart\Seller\Domain\ValueObject\Seller;

interface SellerRepository
{
    public function addSeller(Seller $seller): void;

    public function getSeller(string $id): ?Seller;

    public function deleteSeller(string $id): void;
}