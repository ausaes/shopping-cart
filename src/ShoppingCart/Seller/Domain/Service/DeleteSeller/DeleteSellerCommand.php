<?php

namespace App\ShoppingCart\Seller\Domain\Service\DeleteSeller;

use App\ShoppingCart\Shared\Domain\Bus\Command\Command;

class DeleteSellerCommand implements Command
{
    private string $id;

    public function __construct($id)
    {
        $this->setId($id);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId($id): void
    {

        if (empty($id) || !is_string($id)) {
            throw new \Exception('Invalid Id');
        }
        $this->id = $id;
    }




}
