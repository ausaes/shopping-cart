<?php

namespace App\ShoppingCart\Seller\Domain\Service\DeleteSeller;

use App\ShoppingCart\Seller\Domain\Repository\SellerRepository;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandHandler;

class DeleteSellerCommandHandler implements CommandHandler
{
    private SellerRepository $sellerRepository;

    public function __construct(SellerRepository $sellerRepository)
    {
        $this->sellerRepository = $sellerRepository;
    }

    public function __invoke(DeleteSellerCommand $command)
    {
        if(!$this->sellerRepository->getSeller($command->getId())) {
            throw new \Exception('Seller not found');
        }
        $this->sellerRepository->deleteSeller($command->getId());
    }

}
