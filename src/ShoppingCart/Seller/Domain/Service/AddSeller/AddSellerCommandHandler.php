<?php

namespace App\ShoppingCart\Seller\Domain\Service\AddSeller;

use App\ShoppingCart\Seller\Domain\Repository\SellerRepository;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandHandler;

class AddSellerCommandHandler implements CommandHandler
{
    private SellerRepository $sellerRepository;

    public function __construct(SellerRepository $sellerRepository)
    {
        $this->sellerRepository = $sellerRepository;
    }

    public function __invoke(AddSellerCommand $command)
    {
        if ($this->sellerRepository->getSeller($command->getId())) {
            throw new \Exception('Seller already exists');
        }
        $this->sellerRepository->addSeller(new Seller($command->getId(), $command->getName()));
    }

}
