<?php

namespace App\ShoppingCart\Seller\Domain\Service\AddSeller;

use App\ShoppingCart\Shared\Domain\Bus\Command\Command;

class AddSellerCommand implements Command
{
    private string $id;
    private string $name;

    public function __construct($id, $name)
    {
        $this->setId($id);
        $this->setName($name);
    }

    private function setId($id): void
    {
        if (empty($id) || !is_string($id)) {
            throw new \Exception('Invalid Id');
        }
        $this->id = $id;
    }

    private function setName($name): void
    {
        if (empty($name) || !is_string($name)) {
            throw new \Exception('Invalid name');
        }
        $this->name = $name;
    }


    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
