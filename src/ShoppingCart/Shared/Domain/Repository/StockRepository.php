<?php

namespace App\ShoppingCart\Shared\Domain\Repository;

use App\ShoppingCart\Shared\Domain\ValueObject\Stock;

interface StockRepository
{
    public function linkProductToSeller(string $productId, string $sellerId, float $price): void;

    public function unlinkProductToSeller(string $productId, string $sellerId): void;

    public function getCheapestStockFromProductId(string $productId): ?Stock;

    public function getStockByProductAndSellerId(string $productId, string $sellerId): ?Stock;

    public function updateStock(Stock $stock): void;
}
