<?php

namespace App\ShoppingCart\Shared\Domain\Bus\Command;

interface CommandBus
{
    public function dispatch(Command $command): void;
}