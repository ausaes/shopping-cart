<?php

namespace App\ShoppingCart\Shared\Domain\ValueObject;

use App\ShoppingCart\Product\Domain\ValueObject\Product;
use App\ShoppingCart\Seller\Domain\ValueObject\Seller;

class Stock
{
    private Product $product;
    private Seller $seller;
    private int $quantity;
    private float $price;

    public function __construct(Product $product, Seller $seller, int $quantity, float $price)
    {
        $this->product = $product;
        $this->seller = $seller;
        $this->quantity = $quantity;
        $this->price = $price;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getSeller(): Seller
    {
        return $this->seller;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function isEqualsTo(self $comparator): bool
    {
        return $this->getProduct()->getId() === $comparator->getProduct()->getId() &&
        $this->getSeller()->getId() === $comparator->getSeller()->getId();
    }

}
