<?php

namespace App\ShoppingCart\Shared\Infrastructure\Repository;

use App\ShoppingCart\Product\Domain\Repository\ProductRepository;
use App\ShoppingCart\Seller\Domain\Repository\SellerRepository;
use App\ShoppingCart\Shared\Domain\Repository\StockRepository;
use App\ShoppingCart\Shared\Domain\ValueObject\Stock;

class FileStockRepository implements StockRepository
{
    private string $filename;
    private ProductRepository $productRepository;
    private SellerRepository $sellerRepository;

    public function __construct(string $filename, ProductRepository $productRepository, SellerRepository $sellerRepository)
    {
        $this->filename = $filename;
        $this->productRepository = $productRepository;
        $this->sellerRepository = $sellerRepository;
    }

    public function linkProductToSeller(string $productId, string $sellerId, float $price): void
    {
        $stock = $this->getInventory();
        if (!empty($stock[$sellerId][$productId])) {
            throw new \Exception('Product and seller already linked');
        }
        $stock[$sellerId][$productId] = new Stock(
            $this->productRepository->getProduct($productId),
            $this->sellerRepository->getSeller($sellerId),
            0,
            $price
        );
        $this->setInventory($stock);
    }

    public function unlinkProductToSeller(string $productId, string $sellerId): void
    {
        $stock = $this->getInventory();
        if (empty($stock[$sellerId][$productId])) {
            throw new \Exception('Product and seller are not linked');
        }
        unset($stock[$sellerId][$productId]);
        $this->setInventory($stock);
    }

    public function getCheapestStockFromProductId(string $productId): ?Stock
    {
        $inventory = $this->getInventoryFromProductId($productId);
        if (empty($inventory)) {
            return null;
        }
        usort($inventory, function ($a, $b) {
            return ($a->getPrice() < $b->getPrice()) ? -1 : 1;
        });
        return $inventory[0];
    }

    public function getStockByProductAndSellerId(string $productId, string $sellerId): ?Stock
    {
        $inventory = $this->getInventory();
        return $inventory[$sellerId][$productId] ?? null;
    }

    public function updateStock(Stock $stock): void
    {
        $inventory = $this->getInventory();

        if (!isset($inventory[$stock->getSeller()->getId()][$stock->getProduct()->getId()])) {
            throw new \Exception('Stock not found');
        }
        $inventory[$stock->getSeller()->getId()][$stock->getProduct()->getId()] = $stock;
        $this->setInventory($inventory);
    }

    private function getInventoryFromProductId(string $productId): array
    {
        $productInventory = [];
        foreach ($this->getInventory() as $sellerId => $sellerInventory) {
            if (isset($sellerInventory[$productId])) {
                $productInventory[] = $sellerInventory[$productId];
            }
        }
        return $productInventory;
    }

    private function getInventory(): array
    {
        $linkedProducts = [];
        $file = file_get_contents($this->filename);
        foreach (json_decode($file, true) as $rawLinkedProduct) {
            $linkedProducts[$rawLinkedProduct['sellerId']][$rawLinkedProduct['productId']]
                = new Stock(
                $this->productRepository->getProduct($rawLinkedProduct['productId']),
                $this->sellerRepository->getSeller($rawLinkedProduct['sellerId']),
                (int)$rawLinkedProduct['quantity'],
                $rawLinkedProduct['price']
            );
        }
        return $linkedProducts;
    }

    /**
     * @param Stock[] $inventory
     */
    private function setInventory(array $inventory): void
    {
        $linkedProducts = [];
        foreach ($inventory as $sellerId => $stocks) {
            foreach ($stocks as $stock) {
                $linkedProducts[] = [
                    'sellerId' => $stock->getSeller()->getId(),
                    'productId' => $stock->getProduct()->getId(),
                    'quantity' => $stock->getQuantity(),
                    'price' => $stock->getPrice()
                ];
            }
        }
        $file = fopen($this->filename, 'w');
        fwrite($file, json_encode($linkedProducts));
    }
}
