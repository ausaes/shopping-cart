<?php

namespace App\ShoppingCart\Shared\Infrastructure\Bus\Command;

use App\ShoppingCart\Cart\Domain\Service\AddProductToCart\AddProductToCartCommand;
use App\ShoppingCart\Cart\Domain\Service\AddProductToCart\AddProductToCartCommandHandler;
use App\ShoppingCart\Cart\Domain\Service\ConfirmCart\ConfirmCartCommand;
use App\ShoppingCart\Cart\Domain\Service\ConfirmCart\ConfirmCartCommandHandler;
use App\ShoppingCart\Cart\Domain\Service\DeleteProductFromCart\DeleteProductFromCartCommand;
use App\ShoppingCart\Cart\Domain\Service\DeleteProductFromCart\DeleteProductFromCartCommandHandler;
use App\ShoppingCart\Product\Domain\Service\AddProduct\AddProductCommand;
use App\ShoppingCart\Product\Domain\Service\AddProduct\AddProductCommandHandler;
use App\ShoppingCart\Product\Domain\Service\DecreaseQuantity\DecreaseQuantityCommand;
use App\ShoppingCart\Product\Domain\Service\DecreaseQuantity\DecreaseQuantityCommandHandler;
use App\ShoppingCart\Product\Domain\Service\DeleteProduct\DeleteProductCommand;
use App\ShoppingCart\Product\Domain\Service\DeleteProduct\DeleteProductCommandHandler;
use App\ShoppingCart\Product\Domain\Service\IncreaseQuantity\IncreaseQuantityCommand;
use App\ShoppingCart\Product\Domain\Service\IncreaseQuantity\IncreaseQuantityCommandHandler;
use App\ShoppingCart\Seller\Domain\Service\AddSeller\AddSellerCommand;
use App\ShoppingCart\Seller\Domain\Service\AddSeller\AddSellerCommandHandler;
use App\ShoppingCart\Seller\Domain\Service\DeleteSeller\DeleteSellerCommand;
use App\ShoppingCart\Seller\Domain\Service\DeleteSeller\DeleteSellerCommandHandler;
use App\ShoppingCart\Shared\Domain\Bus\Command\Command;
use App\ShoppingCart\Shared\Domain\Bus\Command\CommandBus;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;

class SymfonyCommandBus implements CommandBus
{
    private MessageBus $bus;

    public function __construct(
        AddSellerCommandHandler $addSellerCommandHandler,
        DeleteSellerCommandHandler $deleteSellerCommandHandler,
        AddProductCommandHandler $addProductCommandHandler,
        DeleteProductCommandHandler $deleteProductCommandHandler,
        AddProductToCartCommandHandler $addProductToCartCommandHandler,
        DeleteProductFromCartCommandHandler $deleteProductFromCartCommandHandler,
        IncreaseQuantityCommandHandler $increaseQuantityCommandHandler,
        DecreaseQuantityCommandHandler $decreaseQuantityCommandHandler,
        ConfirmCartCommandHandler $confirmCartCommandHandler
    ) {
        $this->bus = new MessageBus(
            [
                new HandleMessageMiddleware(
                    new HandlersLocator([
                        AddSellerCommand::class => [$addSellerCommandHandler],
                        DeleteSellerCommand::class => [$deleteSellerCommandHandler],
                        AddProductCommand::class => [$addProductCommandHandler],
                        DeleteProductCommand::class => [$deleteProductCommandHandler],
                        AddProductToCartCommand::class => [$addProductToCartCommandHandler],
                        DeleteProductFromCartCommand::class => [$deleteProductFromCartCommandHandler],
                        IncreaseQuantityCommand::class => [$increaseQuantityCommandHandler],
                        DecreaseQuantityCommand::class => [$decreaseQuantityCommandHandler],
                        ConfirmCartCommand::class => [$confirmCartCommandHandler]
                    ])
                )
            ]
        );
    }

    public function dispatch(Command $command): void
    {
        $this->bus->dispatch($command);
    }
}
