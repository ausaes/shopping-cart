<?php

namespace App\ShoppingCart\Shared\Infrastructure\Bus\Query;

use App\ShoppingCart\Cart\Domain\Service\GetTotalAmount\GetTotalAmountQuery;
use App\ShoppingCart\Cart\Domain\Service\GetTotalAmount\GetTotalAmountQueryHandler;
use App\ShoppingCart\Shared\Domain\Bus\Query\Query;
use App\ShoppingCart\Shared\Domain\Bus\Query\QueryBus;
use App\ShoppingCart\Shared\Domain\Bus\Query\Response;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class SymfonyQueryBus implements QueryBus
{
    private MessageBus $bus;

    public function __construct(
        GetTotalAmountQueryHandler $getTotalAmountQueryHandler
    )
    {
        $this->bus = new MessageBus(
            [
                new HandleMessageMiddleware(
                    new HandlersLocator(
                        [
                            GetTotalAmountQuery::class => [$getTotalAmountQueryHandler]
                        ]
                    )
                )
            ]
        );
    }

    public function ask(Query $query): ?Response
    {
        /** @var HandledStamp $stamp */
        $stamp = $this->bus->dispatch($query)->last(HandledStamp::class);
        return $stamp->getResult();
    }
}
